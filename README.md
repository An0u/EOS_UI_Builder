# EOS_UI_Builder
## Mockup made from scratch for the UI Builder Project
Using Bootstrap and Jekyll 

![alt text](screenshots/ui-builder-screenshot.png "Bootstrap Mockup Prototype")

## Template 

![alt text](images/ui-builder-template.png "Template")